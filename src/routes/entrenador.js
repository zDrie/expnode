const express = require('express');
const router = express.Router();
const enntrenadoresController = require('../controllers/entrenador');



// GET/LEER DATOS
router.get('/', enntrenadoresController.getAll)

router.get('/:id', enntrenadoresController.getOne)

// PUT/ACTUALIZAR DATOS
router.put('/:id', enntrenadoresController.update)

// POST/ALMACENAR DATOS
router.post('/', enntrenadoresController.create)

// DELETE/ELIMINAR DATOS
router.delete('/:id', enntrenadoresController.destroy)







module.exports = router;
