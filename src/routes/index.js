const express = require('express');
const router = express.Router();



router.get('/', (_req, res)=>{
    res.render('index');
});
router.get('/img/pokebolas/:img', (req, res)=>{
    const img = req.params.img;
    return res.sendFile('/src/img/pokebolas/'+img+'.png', { root: '.' });
});
router.get('/img/thumbnails/:img', (req, res)=>{
    const img = req.params.img;
    return res.sendFile('/src/img/thumbnails/'+img, { root: '.' } );
});
router.get('/img/types/:img', (req, res)=>{
    const img = req.params.img;
    return res.sendFile('/src/img/types/'+img, { root: '.' });
});




router.get('/css.css', (req, res)=>{
   // console.log('../src/views/css.css');
    res.sendFile('/src/views/partials/css.css', { root: '.' });
});

router.get('/js.js', (req, res)=>{
     res.sendFile('/src/views/js.js', { root: '.' });
 });

 router.get('/javascript.js', (req, res)=>{
    res.sendFile('/src/views/javascript.js', { root: '.' });
})
 












module.exports = router;