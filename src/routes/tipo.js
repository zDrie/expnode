const express = require('express');
const router = express.Router();
const tiposController = require('../controllers/tipo');
// GET/LEER DATOS
router.get('/', tiposController.getAll)

router.get('/:id', tiposController.getOne)

// PUT/ACTUALIZAR DATOS
router.put('/:id', tiposController.update)

// POST/ALMACENAR DATOS
router.post('/', tiposController.create)

// DELETE/ELIMINAR DATOS
router.delete('/:id', tiposController.destroy)




module.exports = router;
