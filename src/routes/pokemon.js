const express = require('express');
const router = express.Router();
const pokemonesController = require('../controllers/pokemon');


// GET/LEER DATOS
router.get('/', pokemonesController.getAll)

router.get('/:id', pokemonesController.getOne)

//NO OLVIDARSE DE CREAR UNA WEA PA LAS IMAGENES
router.get('/picture/:id', (req, res, next)=>{
//return res.sendFile()
})

// PUT/ACTUALIZAR DATOS
router.put('/:id', pokemonesController.update)

// POST/ALMACENAR DATOS
router.post('/', pokemonesController.create)

// DELETE/ELIMINAR DATOS
router.delete('/:id', pokemonesController.destroy)











module.exports = router;
