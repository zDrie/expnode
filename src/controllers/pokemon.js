const db = require('../models/index');
const Pokemon = db.models.Pokemon;

function getAll(req, res, next) {
    const nombre = req.query.name;
    let find = nombre ? Pokemon.find({
        nombre
    }) : Pokemon.find();

    return find.then(pokemones => {
        return res.status(200).json(pokemones);
    }).catch(next);
};



function getOne(req, res, next) {
    const id = req.params.id;
    if (!id) {
        return res.status(500).json({
            message: 'El id indicado no es valido'
        });
    }
    return Pokemon.findById(id).then(pokemon => {
        if (!pokemon) {
            return res.status(404).json({
                message: '404 Pokemon no encontrado'
            })
        };
        return res.status(200).json(pokemon);
    }).catch(next);
};


function create(req, res, next) {
    const body = req.body;
    for (var i = 0; i < body.length; i++) {
        const nuevoPokemon = {
            id: body[i].id,
            name: body[i].name,
            type: body[i].type,
            base: body[i].base,
            thumbnail: body[i].thumbnail
        };
        Pokemon.create(nuevoPokemon);
    }
    return res.status(200).json('pokemones cargados atr')

}

function update(req, res, next) {
    const id = req.params.id;
    return Pokemon.findOneAndUpdate({
        _id: id
    }, {
        $set: req.body
    }, {
        new: true
    }).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);
}

function destroy(req, res, next) {
    const id = req.params.id;

    return Pokemon.findOneAndDelete({
        _id: id
    }).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);
}

module.exports = {
    getAll,
    getOne,
    create,
    update,
    destroy
};