

const db = require('../models/index');
const Entrenador = db.models.Entrenador;

function getAll(req, res, next) {
const email = req.query.email;
let find = email ? Entrenador.find({email}) : Entrenador.find();

    return find.then(entrenadores => {
        return res.status(200).json(entrenadores);
    }).catch(next);
}




function getOne(req, res, next) {
    const id = req.params.id;
    if (!id) {
        return res.status(500).json({
            message: 'El id indicado no es valido'
        });
    }
    return Entrenador.findById(id).populate('pokemonesObtenidos').then(ntrenador => {
        if (!ntrenador) {
            return res.status(404).json({
                message: '404 Entrenador no encontrado'
            })
        }
        return res.status(200).json(ntrenador);
    }).catch(next);
}


function create(req, res,next){
    const body = req.body;
    const nuevoEntrenador = {
        nombre: body.nombre,
        cantMedallas: body.cantMedallas,
        pokemonesObtenidos: body.pokemonesObtenidos
    };
    return Entrenador.create(nuevoEntrenador).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);
}

function update(req, res,next){
    const id = req.params.id;
    return Entrenador.findOneAndUpdate({_id: id}, {$set: req.body}, {new: true}).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);
}

function destroy(req, res,next){
    const id = req.params.id;
    
    return Entrenador.findOneAndDelete({_id: id}).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);
}

module.exports = {
    getAll,
    getOne,
    create,
    update,
    destroy
};