const db = require('../models/index');
const Tipo = db.models.Tipo;

function getAll(req, res, next) {
const type = req.query.type;
let find = type ? Tipo.find({type}) : Tipo.find();

    return find.then(tipos => {
        return res.status(200).json(tipos);
    }).catch(next);
}




function getOne(req, res, next) {
    const id = req.params.id;
    if (!id) {
        return res.status(500).json({
            message: 'El id indicado no es valido'
        });
    }
    return Tipo.findById(id).then(tipos => {
        if (!tipos) {
            return res.status(404).json({
                message: '404 Tipo no encontrado'
            })
        }
        return res.status(200).json(tipos);
    }).catch(next);
}


function create(req, res,next){
    const body = req.body;
    const nuevoTipo = {
        type: body.type,
        fuerte: body.fuerte,
        debil: body.debil,
        resistente: body.resistente,
        vulnerable: body.vulnerable,
        img: body.img
    };
    return Tipo.create(nuevoTipo).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);

}

function update(req, res,next){
    const id = req.params.id;
    return Tipo.findOneAndUpdate({_id: id}, {$set: req.body}, {new: true}).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);

}

function destroy(req, res,next){
    const id = req.params.id;
    
    return Tipo.findOneAndDelete({_id: id}).then(ent => {
        return res.status(200).json(ent);
    }).catch(next);

}

module.exports = {
    getAll,
    getOne,
    create,
    update,
    destroy
};