const express = require('express');
const path = require('path');
const morgan = require('morgan');
const db = require('./models');
const connectDB = db.connectDb;




const app = express();
//conectando a la base de datos


//importando rutas
const indexRoutes = require('./routes/index');
const entrenadoresRouter = require('./routes/entrenador');
const pokemonsRouter = require('./routes/pokemon');
const tiposRouter = require('./routes/tipo');



//settings
app.set('port', process.env.PORT || 9001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//middleware
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static('views'));

//routes
app.use('/', indexRoutes);
app.use('/entrenadores', entrenadoresRouter);
app.use('/pokemones', pokemonsRouter);
app.use('/tipos', tiposRouter);

//errores
app.use(function(_req, _res, next){
    next(createError(404));
});
app.use(function(err, req, res, _next){
const error = err.message;
if(req.app.get('env') === 'development') console.error(err);
res.status(err.status || 500).json(error);
});


//starting the server
connectDB().then(()=>{
    app.listen(app.get('port'), ()=>{
        console.log('Server ON Port'.red, app.get('port'));
    
    });
})




