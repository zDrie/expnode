$(document).ready(function () {

    var url = '/tipos'
    var url2 = '/pokemones'
    var request = new XMLHttpRequest();
    var request2 = new XMLHttpRequest();

    //request a tipos.json
    request.open('GET', url);
    request.responseType = 'json';
    request.send();
    request.onload = function () {
        if (request.status != 200) {
            alert("Error al intentar consultar tipos.json");
            return;
        }
        var pato = request.response;
        agregarOptions(pato);

    }

    //request a pokedex.json
    request2.open('GET', url2);
    request2.responseType = 'json';
    request2.send();
    request2.onload = function () {
        if (request2.status != 200) {
            alert("Error al intentar consultar pokedex.json");
            return;
        }
        var pokedex = request2.response;
        mostrarResultados(pokedex, request.response);

    }


    /**
     * agrega opciones a los selectores de tipos, tantos como haya en tipos.json
     * @param {Array: string} arrayTipos 
     */
    function agregarOptions(arrayTipos) {
        var tipo1 = $("#tipo1");
        var tipo2 = $("#tipo2");
        arrayTipos.forEach(tipo => {
            tipo1.append("<option value=" + tipo.type + ">" + tipo.type + "</option>");
            tipo2.append("<option value=" + tipo.type + ">" + tipo.type + "</option>");
        })

    }


    /**
     * permite obtener un array con los datos a medida que se van modificando
     */
    var resultados = [];
    var input = document.querySelectorAll('input, select');
    for (var i = 0; i < input.length; i++) {
        input[i].addEventListener("change", function () {
            resultados = capturarDatos();

        })
    }


    /**
     * cuando se hace click en el boton buscar, se realiza el filtrado de acuerdo a lo que se haya ingresado en el formulario
     */
    var buscar = $('#buscar');
    buscar.click(function () {
        var arrayResult = [];
        arrayResult = filtrar(request2.response, resultados[0], resultados[1], resultados[2], resultados[3], resultados[4], resultados[5], resultados[6], resultados[7], resultados[8], resultados[9], resultados[10], resultados[11], resultados[12], resultados[13], resultados[14]);
        mostrarResultados(arrayResult, request.response);

    });


    /**
     * Capturo los datos desde el formulario y los retorno como un array, es llamado por el listener (input)
     */
    function capturarDatos() {
        var tipo1 = document.getElementById("tipo1").value;
        var tipo2 = document.getElementById("tipo2").value;
        var AttackMin = document.getElementById("AttackMin").value;
        var AttackMax = document.getElementById("AttackMax").value;
        var DefMin = document.getElementById("DefMin").value;
        var DefMax = document.getElementById("DefMax").value;
        var HPMin = document.getElementById("HPMin").value;
        var HPMax = document.getElementById("HPMax").value;
        var sAttackMin = document.getElementById("sAttackMin").value;
        var sAttackMax = document.getElementById("sAttackMax").value;
        var sDefMin = document.getElementById("sDefMin").value;
        var sDefMax = document.getElementById("sDefMax").value;
        var SpeedMax = document.getElementById("SpeedMax").value;
        var SpeedMin = document.getElementById("SpeedMin").value;
        var pokeNombre = document.getElementById("pokeNombre").value;
        var r = [tipo1, tipo2, pokeNombre, AttackMin, AttackMax, DefMin, DefMax, HPMin, HPMax, sAttackMin, sAttackMax, sDefMin, sDefMax, SpeedMin, SpeedMax];
        return r;
    }


    /**
     * Filtro el array por las condiciones establecidas, es llamado en buscar.click
     * @param {Array} dataArray 
     * @param {string} tipo1
     * @param {string} tipo2
     * @param {string} nombre 
     * @param {number} ataqueMin 
     * @param {number} ataqueMax 
     * @param {number} defMin
     * @param {number} defMax
     * @param {number} hpMin
     * @param {number} hpMax
     * @param {number} sAttackMin
     * @param {number} sAttackMax
     * @param {number} sDefMin
     * @param {number} sDefMax
     * @param {number} speedMin
     * @param {number} speedMax
     */
    function filtrar(dataArray, tipo1, tipo2, nombre, ataqueMin, ataqueMax, defMin, defMax, hpMin, hpMax, sAttackMin, sAttackMax, sDefMin, sDefMax, speedMin, speedMax) {
        let resultado = [];
        dataArray.forEach(element => {
            if ((!tipo1 || compararTipos(element.type, tipo1)) &&
                (!tipo2 || compararTipos(element.type, tipo2)) &&
                (!nombre || element.name.toLowerCase().indexOf(nombre) >= 0) &&
                (!ataqueMin || element.base.Attack >= ataqueMin) &&
                (!ataqueMax || element.base.Attack <= ataqueMax) &&
                (!defMin || element.base.Defense >= defMin) &&
                (!defMax || element.base.Defense <= defMax) &&
                (!hpMin || element.base.HP >= hpMin) &&
                (!hpMax || element.base.HP <= hpMax) &&
                (!sAttackMin || element.base.Sp.Attack >= sAttackMin) &&
                (!sAttackMax || element.base.Sp.Attack <= sAttackMax) &&
                (!sDefMin || element.base.Sp.Defense >= sDefcMin) &&
                (!sDefMax || element.base.Sp.Defense <= sDefcMax) &&
                (!speedMin || element.base.Speed >= speedMin) &&
                (!speedMax || element.base.Speed <= speedMax)
            ) { //Si especificó ataqueMax
                resultado.push(element);
            }
        });
        return resultado;
    }


    /**
     * Determina si un tipo se encuentra en un array, sirve para filtrar por tipo
     * @param {Array: string} elementType 
     * @param {string} tipoComparar 
     */
    function compararTipos(elementType, tipoComparar) {
        var x = false;
        elementType.forEach(elemento => {
            if (elemento == tipoComparar) {
                x = true;
            }
        })
        return x;
    }


    /**
     * Muestra un array de pokemons en la pantalla (previamente filtrados o no)
     * @param {Array} dataArray 
     */
    function mostrarResultados(dataArray, tiposArray) {
        items = document.getElementById("contenido");
        items.innerHTML = "";

        dataArray.forEach(pokemon => {

            //Creo el div "name"
            let divName = document.createElement('div');
            divName.setAttribute("class", "text-center");
            divName.setAttribute("id", "divname");
            divName.innerHTML = "<h5> #" + pokemon.id + " " + pokemon.name + "</h5>";

            //Creo un ul para las estadísticas 
            let ulStats = document.createElement("ul");
            ulStats.setAttribute("class", "stats");

            //Creo un list item por cada type
            let divType = document.createElement("div");
            divType.setAttribute("class", "row justify-content-center mt-1");

            pokemon.type.forEach(type => {
                let imagenTipo = document.createElement('img');
                imagenTipo.setAttribute("href", "#");
                imagenTipo.setAttribute("data-toggle", "popover");
                imagenTipo.setAttribute("role", "button");
                imagenTipo.setAttribute("data-trigger", "focus");
                imagenTipo.setAttribute("tabindex", 0);

                tiposArray.forEach(element => {
                    if (element.type == type) {

                        imagenTipo.setAttribute("src", "../"+element.img);
                        imagenTipo.setAttribute("title", type);
                        imagenTipo.setAttribute("class", "ml-1 img-responsive btn");
                        imagenTipo.setAttribute("data-content", "Fuerte contra: " + element.fuerte +
                            "\n" + "Débil contra: " + element.debil +
                            "\n" + "Resistente a: " + element.resistente +
                            "\n" + "Vulnerable a: " + element.vulnerable);

                        divType.append(imagenTipo);

                    }
                })

            });

            //Agrego HP
            let liHP = document.createElement("li");
            liHP.innerText = "HP: " + pokemon.base.HP;
            ulStats.append(liHP);

            //Agrego Attack
            let liAttack = document.createElement("li");
            liAttack.innerText = "Ataque:" + pokemon.base.Attack;
            ulStats.append(liAttack);

            //Agrego Defense
            let liDefense = document.createElement("li");
            liDefense.innerText = "Defensa: " + pokemon.base.Defense;
            ulStats.append(liDefense);

            //Agrego Speed
            let liSpeed = document.createElement("li");
            liSpeed.innerText = "Velocidad: " + pokemon.base.Speed;
            ulStats.append(liSpeed);

            //Agrego Speed Attack
            let lisAttack = document.createElement("li");
            lisAttack.innerText = "Ataque Especial: " + pokemon.base["SpAttack"];
            ulStats.append(lisAttack);

            //Agrego Speed Defense
            let lisDef = document.createElement("li");
            lisDef.innerText = "Defensa Especial: " + pokemon.base["SpDefense"];
            ulStats.append(lisDef);

            //Creo el elemento a que contendrá cada pokemon
            let a = document.createElement('a');
            a.setAttribute("class", "rounded mt-2 mr-2 col col-lg-2 col-md-3 col-sm-4 col-xs-4 ");

            //Creo el div contenedor
            let divContainer = document.createElement('div');
            divContainer.setAttribute("class", "contenedor");

            //Meto dentro del tag a un contenedor
            a.append(divContainer);

            //Creo un elemento imagen y le asigno el thumbnail
            let div1 = document.createElement('div');
            div1.setAttribute('class', 'row justify-content-center');
            let img = document.createElement('img');
            img.setAttribute('src', '../' + pokemon.thumbnail);
            img.setAttribute('class', 'mt-1 ml-1 mr-1');
            div1.append(img);

            //Meto dentro del contenedor todos los elementos
            divContainer.append(div1);
            divContainer.append(divName);
            divContainer.append(divType);
            divContainer.appendChild(ulStats);

            items.append(a);

            $(function () {
                $('[data-toggle="popover"]').popover({
                    trigger: 'hover'
                })
            });


        });

    }

})