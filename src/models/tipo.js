const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Tipo = new Schema({
    "type": String,
    "fuerte":  [String],
    "debil":  [String],
    "resistente": [String],
    "vulnerable": [String],
    "img": String
});

module.exports = mongoose.model('Tipo', Tipo, 'Tipos');