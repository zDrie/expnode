const colors = require('colors');
const mongoose = require('mongoose');
const Entrenador = require('./entrenador');
const Pokemon = require('./pokemon');
const Tipo = require('./tipo');

const connectDb = () => {
  return mongoose.connect('mongodb://localhost/expnodemon', {
            // useNewUrlParser: true, 
            useUnifiedTopology: true
        })
}

const models = {
    Entrenador,
    Pokemon,
    Tipo
}
module.exports ={ 
connectDb,
models
}