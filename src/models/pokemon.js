const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Pokemon = new Schema({
    "id": Number,
    "name": String,
    "type": [String],
    "base": {
        "HP": Number,
        "Attack": Number,
        "Defense": Number,
        "SpAttack": Number,
        "SpDefense": Number,
        "Speed": Number
    },
    "thumbnail": String
});

module.exports = mongoose.model('Pokemon', Pokemon, 'Pokemones')