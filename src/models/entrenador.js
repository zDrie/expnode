const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Entrenador = new Schema({
    nombre: String,
    cantMedallas: {
        type: Number,
        default: 0
    },
    pokemonesObtenidos: [{
        type: Schema.Types.ObjectId,
        ref: 'Pokemon',
        default: []
    }]
});

module.exports = mongoose.model('Entrenador', Entrenador, "Entrenadores");